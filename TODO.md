# The Annotated L5R (TODO)

## General
* When printing with background, the background doesn't repeat.
* Figure out how to get GitLab pages working.
* Figure out how we're going to organize card art.
* Write the FAQ and the Sources page. Add to header?
* Write the index.html page.
* Permalinks should be hidden when printed.

## Fiction
* ffg/the-story-so-far: Need to add and identify images.
* Actually, you know, transcribe some annotations (ha. ha.)
